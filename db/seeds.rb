# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'

def load_facilities

  data_map = { facility_type: :facility ,
   facility_number: :facility,
   facility_name: :facility,
   licensee: :facility,
   user_id: :facility,
   telephone_number: :address,
   street: :address,
   city: :address,
   state: :address,
   zip: :address,
   county_name: :address,
   regional_office: :facility,
   facility_capacity: :facility,
   facility_status: :facility,
   license_first_date: :facility,
   closed_date: :facility
  }


  puts File.expand_path('miscs/facilities.csv')

  processed_row = 0
  CSV.foreach("miscs/facilities.csv") do |row|
    processed_row += 1
    next if processed_row == 1
    print("#{processed_row} ... ")
    facility = Facility.new
    address  =Address.new

    idx = 0
    data_map.each do |k,v|
      eval(v.to_s).send("#{k}=", row[idx])
      idx += 1
    end

    facility.save!
    address.owner = facility
    address.save!
  end

  puts("\n\n Finish loading facilities and addresses.")
end


def create_roles
  Role::ROLES.each do |k,v|
    Role.create(name: v, description: "Role for #{k}")
  end
end


def create_users
  addresses = [
    { 
      street: '386 W Andrews Ave',
      city: 'Rancho Cucamonga',
      state: 'CA',
      zip: '93722',
      county_name: 'San Bernardino',
      telephone_number: '(909) 764-3147'

    },

    { 
      street: '275 Industrial Road',
      city: 'Camarillo',
      state: 'CA',
      zip: '93012',
      county_name: 'Ventura',
      telephone_number: '(805) 158-2059'

    },

    { 
      street: '49 Broadway street',
      city: 'Orange',
      state: 'CA',
      zip: '92868',
      county_name: 'Orange',
      telephone_number: '(714) 269-3160'

    }
  ]

  users = [
    {
      email: 'parent_1@hanoian.com',
      password: 'development', 
      first_name: 'Clara',
      last_name: 'Page'
    },

    {
      email: 'parent_2@hanoian.com',
      password: 'development', 
      first_name: 'Daisy',
      last_name: 'Walsh'
    },

    {
      email: 'parent_3@hanoian.com',
      password: 'development', 
      first_name: 'Martin',
      last_name: 'Harrison'
    }

  ]

  role = Role.find_by_name(Role::ROLES[:parent])

  3.times do |i|
    user = User.new(users[i])
    user.roles << role
    user.save!
    address = Address.new(addresses[i])
    address.owner = user
    address.save!
  end

end


# Child has one facility. Address of children will be facility address
def create_facilities_people
  zips = ['93722', '93012', '92868']
  addresses = Address.includes(:owner).where("zip in ('#{zips.join('\',\'')}') AND owner_type='Facility'").to_a
  facilities = addresses.map(&:owner)
  child_role          = Role.find_by_name(Role::ROLES[:child])
  facility_admin_role = Role.find_by_name(Role::ROLES[:facility_admin])
  case_worker_role    = Role.find_by_name(Role::ROLES[:case_worker])

  puts("\n\nCreate children")
  processed_row = 0
  facility_idx = 0
  CSV.foreach("miscs/children.csv") do |row|
    processed_row += 1
    print("#{processed_row} ... ")
    
    user = User.new({
              email: "child_#{processed_row}@hanoian.com",
              password: 'development',
              first_name: row[0],
              last_name: row[1]
           })


    user.roles << child_role
    user.facility = facilities[facility_idx]
    user.save!

    facility_idx += 1 if (processed_row % 10 == 0)
  end #create children

  puts("\n\nCreate facility admins")
  processed_row = 0
  facility_idx = 0
  CSV.foreach("miscs/facility_admins.csv") do |row|
    processed_row += 1
    print("#{processed_row} ... ")
    
    user = User.new({
              email: "facility_admin_#{processed_row}@hanoian.com",
              password: 'development',
              first_name: row[0],
              last_name: row[1]
           })


    user.roles << facility_admin_role
    user.facility = facilities[facility_idx]
    user.save!
    facilities[facility_idx].administrator = user
    facilities[facility_idx].save!

    facility_idx += 1
  end #create facility admins

  puts("\n\nCreate case worker")
  processed_row = 0
  facility_idx = 0
  CSV.foreach("miscs/case_workers.csv") do |row|
    processed_row += 1
    print("#{processed_row} ... ")
    
    user = User.new({
              email: "case_worker_#{processed_row}@hanoian.com",
              password: 'development',
              first_name: row[0],
              last_name: row[1]
           })


    user.roles << case_worker_role
    user.facility = facilities[facility_idx]
    user.save!

    facility_idx += 1 if (processed_row % 2 == 0)
  end #create case workers

  puts("\n\n Finish create_facilities_people.\n\n")
end

load_facilities
create_roles
create_users
create_facilities_people