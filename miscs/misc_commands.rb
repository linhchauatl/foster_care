# https://chhs.data.ca.gov/Facilities-and-Services/Community-Care-Licensing-Foster-Family-Agency-Loca/v9bn-m9p9
require 'csv'
headers = []
count = 0
CSV.foreach("/Users/lchau/projects/foster_care/miscs/foster.csv") do |row|
  puts row.inspect
  headers = row if count == 0
  count += 1
  break if count > 3
end


data_keys = []
headers.each do |header|
  #puts header.gsub(' ', '').underscore
  data_keys << header.gsub(' ', '').underscore
end

["facility_type", "facility_number", "facility_name", "licensee", "facility_administrator", "facility_telephone_number", "facility_address", "facility_city", "facility_state", "facility_zip", "county_name", "regional_office", "facility_capacity", "facility_status", "license_first_date", "closed_date", "location"]

# user_id is administrator
rails g model Facility facility_type:string facility_number:integer facility_name:string licensee:string user_id:integer \
      regional_office:integer facility_capacity:integer facility_status:string license_first_date:timestamp closed_date:timestamp



rails generate devise:install     # Creates config file, etc.
rails generate devise user        # Create model class, routes, etc.
rake db:migrate                   # Create user table
rails generate devise:views users

rails g controller facilities

# user_id is administrator
rails g model Facility facility_type:string facility_number:integer facility_name:string licensee:string user_id:integer \
      regional_office:integer facility_capacity:integer facility_status:string license_first_date:timestamp closed_date:timestamp

# One facility has many address
rails g model Address street:string city:string state:string zip:string county_name:string telephone_number:string owner_id:integer

rails g model Role name:string description:string

# One facility has many children
rails g migration AddFacilityIdToUsers facility_id:integer

# User has many roles and Role has many users
rails g model RolesUser role_id:integer user_id:integer

rails g migration AddNameToUsers first_name:string last_name:string


rails g model Message subject:string content:text sender:references
rails g model MessagesUser message:references user:references read_status:boolean

rails g controller messages


==================================================================================================================================================

docker build -t foster_care_ubuntu:latest .

http://www.convertcsv.com/generate-test-data.htm
first, last, email, natural(5000), street, city, state, zip, phone

ar = Address.all.group_by(&:zip).map { |k,v|  { k => v.size }  }

User.all.map { |x| x.addresses.first.zip }

zips = ['93722', '93012', '92868']

addresses = Address.where('zip in (?)', zips).to_a # 23 addresses


rails g controller users


include Devise::TestHelpers
def current_user
  @user = User.first
  request.session[:user] = @user.id
  @user
end


<a rel="nofollow" data-method="delete" href="/users/sign_out">Logout</a>

http://bitoptech.org/users

rpm -Uvh http://yum.postgresql.org/9.5/redhat/rhel-7-x86_64/pgdg-centos94-9.5-1.noarch.rpm

bundle exec puma -b 'unix:///var/www/foster_care/shared/tmp/sockets/foster_care.sock'  -e staging  --control 'unix:////var/www/foster_care/shared/tmp/sockets/foster_care_ctl.sock' -d
bundle exec puma -b 'unix:///var/www/foster_care/shared/tmp/sockets/foster_care.sock'  -e production  --control 'unix:////var/www/foster_care/shared/tmp/sockets/foster_care_ctl.sock' -d

cap staging puma:safe_restart
cap production puma:safe_restart

# This require pem password
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 3650

# This does not require pem password
openssl req -nodes -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 3650

psql -U postgres -W -a -d foster_care_production -f db/sample_postgresql_data.sql

pg_dump  -U postgres -F c -b -v -f db/sample_postgresql_data foster_care_development
pg_restore -U postgres -W  sample_postgresql_data -d foster_care_production db/sample_postgresql_data

