class UsersController < ApplicationController
  before_action :authenticate_user!

  def show
    @user = current_user
  end

  def edit
  end

  def update
    @user = current_user

    user_info = params[:user]

    if (user_info[:password] != user_info[:password_confirmation])
      @status_message = 'Password and Password Confirmation do not match.'
      @bg_class = 'bg-danger'
      render template: 'users/show'
      return
    end

    if (user_info[:password].blank?)
      user_info.reject! { |k, v| k.to_s =~ /^password/ }
    end


    address_info = params[:address]
    if (!valid_address_info?(address_info))
      @status_message = 'Address information is not valid.'
      @bg_class = 'bg-danger'
      render template: 'users/show'
      return
    end

    user_info.each do |k, v|
      @user.send("#{k}=", v)
    end
    @user.save!

    address = @user.addresses.first
    if !address
      address = Address.new
      address.owner = @user
    end
    
    address_info.each do |k, v|
      address.send("#{k}=", v)
    end
    address.save!
    
    @status_message = 'User updated.'
    @bg_class = 'bg-success'
    render template: 'users/show'
  end

  def search
    search_param = params[:q]
    users = User.where("email like '#{search_param.downcase}%' OR first_name like '#{search_param.capitalize}%'").to_a

    results = users.map { |x| {name: x.name, email: x.email } }
    render json: { items: results}
  end

  private
  def valid_address_info?(address_info)
    valid = true
    %w( street city state zip telephone_number).each do |attr|
      valid = false if  address_info[attr.to_sym].blank?
    end
    valid
  end

end

