class FacilitiesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :zip_search, :search]

  def index
    @facilities = Facility.includes(:addresses).all
  end

  def zip_search
    search_param = params[:q]
    zips = Address.where("zip like '#{search_param.downcase}%'").map(&:zip).to_a.uniq.sort

    results = zips.map { |x| {text: x, id: x } }
    render json: { items: results}
  end

  def search
    zipcodes = params[:zipcodes]
    if (!zipcodes)
      @facilities = Facility.includes(:addresses).where(facility_status: 'LICENSED')
    else
      facilities = Address.includes( owner: :addresses ).where("zip in (?) AND owner_type = 'Facility'", zipcodes).to_a.group_by(&:owner).keys
      @facilities = facilities.reject { |x| x.facility_status != 'LICENSED' }
    end
    render template: '/facilities/index'
  end
end



