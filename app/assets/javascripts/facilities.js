/* Run on load */
ready = function() {
  $('#facilities_table').DataTable( {
      columnDefs: [ 
        {
          "targets": 0,
          "orderable": false
        },
        {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }, 
        {
            targets: [ 2 ],
            orderData: [ 2, 0 ]
        }, 
        {
            targets: [ 3 ],
            orderData: [ 3, 0 ]
        },
        {
            targets: [ 12 ],
            orderData: [ 12, 0, 1 ]
        }

      ],
      "oLanguage": {
        "sSearch": "<span>Filter by:</span> _INPUT_" //search
      },
      "oSearch": {"sSearch": "LICENSED"}
  }); // data table

  $('#zipcode_multi_select').select2({
    ajax: {
      headers: { "X-CSRFToken": "<%= csrf_meta_tags %>" },
      url: "/facilities/zip_search",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
        };
       
      },
      cache: true
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
   
  }); //user_multi_select


};

// Turbo link has its own event
$(document).ready(ready)
$(document).on('page:load', ready)