message_ready = function() {
  // $('#language_multi_select').val() give you selected values
  $('#user_multi_select').select2({
    ajax: {
      headers: { "X-CSRFToken": "<%= csrf_meta_tags %>" },
      url: "/recipients/search",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        var select2Data = $.map(data.items, function (obj) {
            obj.id = obj.email;
            obj.text = obj.name + '(' + obj.email + ')';

            return obj;
        });
        return {
            results: select2Data,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
        };
       
      },
      cache: true
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 2,
   
  }); //user_multi_select

};

// Turbo link has its own event
$(document).ready(message_ready)
$(document).on('page:load', message_ready)