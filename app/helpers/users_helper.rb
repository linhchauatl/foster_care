module UsersHelper
  def input_type(field_name)
    return 'password' if (field_name =~ /^password/ )
    return 'text'
  end
end
