class Message < ActiveRecord::Base
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'

  has_many :messages_users, dependent: :destroy
  has_many :recipients, through: :messages_users

  def recipients_array
    recipients.map { |x| "#{x.name} (#{x.email})" }
  end

  def sent_date
    created_at.in_time_zone('Pacific Time (US & Canada)').strftime('%b-%d-%Y %I:%M:%S %p %z')
  end
end
