Rails.application.routes.draw do
  devise_for :users
  root 'facilities#index'
  resources :facilities, only: [:index]
  resources :users, only: [:show, :edit]
  resources :messages

  get '/recipients/search' => 'users#search'
  put '/profiles'          => 'users#update'
  get '/facilities/search' => 'facilities#search'
  get '/facilities/zip_search' => 'facilities#zip_search'
end
